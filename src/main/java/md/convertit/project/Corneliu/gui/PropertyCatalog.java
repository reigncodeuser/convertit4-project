package md.convertit.project.Corneliu.gui;

import md.convertit.project.Corneliu.model.Address;
import md.convertit.project.Corneliu.model.Property;
import md.convertit.project.Corneliu.services.ExcelService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.time.LocalDate;
import java.util.*;
import java.util.List;

/**
 * Created by Cornel on 25.06.2017.
 */
public class PropertyCatalog extends JFrame {

    private static final int FIELD_COLUMNS = 15;
    private JTextField jTextFieldTypeOfProperty;
    private JComboBox jComboBoxCountRooms;
    private JTextField jTextFieldPrice;
    private JTextField jTextFieldAddressName;
    private JTextField jTextFieldAddressNumber;
    private JRadioButton jRadioButtonSoldYes;
    private ButtonGroup group;
    private JRadioButton jRadioButtonSoldNo;
    private JTextField jTextFieldSoldDate;
    private JButton jButtonSave;
    private JButton jButtonClear;
    private JSplitPane jSplitPane;
    private JPanel leftPanel;
    private PropertyListPanel rightPanel;

    //  Menu
    private JMenuItem itemExit;
    private JMenuItem itemAbout;

    private JMenuItem itemExcelExport;
    private JMenuItem itemDatabaseExport;

    private JMenuItem itemExcelImport;
    private JMenuItem itemDatabaseImport;

    public PropertyCatalog() throws HeadlessException {
        initWindow();
        initSplitPane();
        initListeners();
        initMenuListeners();
    }

    private void initListeners() {

        jButtonSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean isFormValid = validateForm();

                if (isFormValid) {
                    saveProperty();
                }
            }
        });

        jButtonClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearForm();
            }
        });

        itemExcelExport.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ExcelService excelService = new ExcelService();

                List<Property> list = rightPanel.getPropertyList();

                excelService.export(list, "Property.xls");
            }
        });

        itemExcelImport.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ExcelService excelService = new ExcelService();

                excelService.importFromExcel("Property.xls");

                excelService.getPropertyList();
                for (int i = 0; i < excelService.getPropertyList().size(); i++) {
                    rightPanel.saveProperty(excelService.getPropertyList().get(i));
                }
            }
        });
    }

    private void saveProperty() {
        Property property = new Property();

        String typeOfProperty = jTextFieldTypeOfProperty.getText();
        property.setTypeOfProperty(typeOfProperty);

        String countRooms = (String) jComboBoxCountRooms.getSelectedItem();
        if (!countRooms.equals("-")) {
            property.setCountRooms(Integer.parseInt(countRooms));
        }

        double price = Double.parseDouble(jTextFieldPrice.getText());
        property.setPrice(price);

        String addressName = jTextFieldAddressName.getText();

        int addressNumber = Integer.parseInt(jTextFieldAddressNumber.getText());

        Address address = new Address(addressName, addressNumber);

        property.setAdress(address);

        boolean isSold;

        if (jRadioButtonSoldYes.isSelected()) {
            isSold = true;
        } else {
            isSold = false;
        }

        property.setSold(isSold);


        if (jRadioButtonSoldYes.isSelected()) {

            String soldDateString = jTextFieldSoldDate.getText();
            //split in year,mouth,day
            String[] date = soldDateString.split("\\.");

            int day = Integer.parseInt(date[0]);
            int month = Integer.parseInt(date[1]);
            int year = Integer.parseInt(date[2]);

            LocalDate soldDate = LocalDate.of(year, month, day);
            property.setSoldDate(soldDate);
        }


//        System.out.println(property);

        rightPanel.saveProperty(property);
        clearForm();
    }

    private void clearForm() {
        jTextFieldSoldDate.setText("");
        jTextFieldAddressName.setText("");
        jTextFieldAddressNumber.setText("");
        jTextFieldPrice.setText("");
        jTextFieldTypeOfProperty.setText("");
        jComboBoxCountRooms.setSelectedIndex(0);
        group.clearSelection();
    }

    private boolean validateForm() {
        boolean isValid = true;
        StringBuilder builder = new StringBuilder();
        builder.append("Errors occurred:\n");

        //text fields validate
        String typeProperty = jTextFieldTypeOfProperty.getText();
        if (typeProperty.length() == 0 || typeProperty.length() > 15) {
            isValid = false;
            String message = "Property's Type must contain 1 to 15 letters";
            builder.append(message + "\n");
        }

        String addressName = jTextFieldAddressName.getText();
        if (addressName.length() == 0 || addressName.length() > 30) {
            isValid = false;
            String message = "Address Name must contain 1 to 30 letters";
            builder.append(message + "\n");
        }

        int addressNumber = Integer.parseInt(jTextFieldAddressNumber.getText());
        if (addressNumber == 0 || addressNumber > 99) {
            isValid = false;
            String message = "Address Number must contain 1 to 99";
            builder.append(message + "\n");
        }

        String priceAsText = jTextFieldPrice.getText();
        try {
            Double.parseDouble(priceAsText);
        } catch (NumberFormatException e) {
            isValid = false;
            builder.append("Price is invalid!");
        }

        String datePattern = "\\d{2}.\\d{2}.\\d{4}";
        String date = jTextFieldSoldDate.getText();
        if (jRadioButtonSoldYes.isSelected()) {

            if (!date.matches(datePattern)) {
                isValid = false;
                String message = "Introduceti data dupa formatul (ZZ.LL.AAAA)";
                builder.append(message + "\n");
            }
        }

        if (!isValid) {
            JOptionPane.showMessageDialog(this, builder.toString(),
                    "Warning", JOptionPane.ERROR_MESSAGE);
        }

        return isValid;
    }


    private void initSplitPane() {
        jSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        leftPanel = getLeftPanel();
        jSplitPane.setLeftComponent(leftPanel);

        rightPanel = new PropertyListPanel(this);
        jSplitPane.setRightComponent(rightPanel);

        jSplitPane.setDividerLocation(250);
        jSplitPane.setEnabled(false);
        getContentPane().add(jSplitPane);
    }

    private JPanel getLeftPanel() {
        JPanel jPanel = new JPanel();
        BoxLayout layoutMain = new BoxLayout(jPanel, BoxLayout.Y_AXIS);
        jPanel.setLayout(layoutMain);

        jPanel.add(new JLabel("Property's Type"));
        jTextFieldTypeOfProperty = new JTextField(FIELD_COLUMNS);
        jTextFieldTypeOfProperty.setMaximumSize(jTextFieldTypeOfProperty.getPreferredSize());
        jPanel.add(jTextFieldTypeOfProperty);

        String countRooms[] = {"-", "1", "2", "3", "4", "5"};

        JPanel jPanelRooms = new JPanel();
        BoxLayout layoutRooms = new BoxLayout(jPanelRooms, BoxLayout.X_AXIS);
        jPanelRooms.setLayout(layoutRooms);
        jPanelRooms.add(new JLabel("Rooms "));
        jComboBoxCountRooms = new JComboBox(countRooms);
        jComboBoxCountRooms.setMaximumSize(jComboBoxCountRooms.getPreferredSize());
        jPanelRooms.add(jComboBoxCountRooms);
        jPanel.add(jPanelRooms);

        jPanel.add(new JLabel("Price"));
        jTextFieldPrice = new JTextField(10);
        jTextFieldPrice.setMaximumSize(jTextFieldPrice.getPreferredSize());
        jPanel.add(jTextFieldPrice);

        JPanel jPanelAddress = new JPanel();
        BoxLayout layoutAddress = new BoxLayout(jPanelAddress, BoxLayout.X_AXIS);
        jPanelAddress.setLayout(layoutAddress);

        JPanel jPanelAddressLeft = new JPanel();
        layoutAddress = new BoxLayout(jPanelAddressLeft, BoxLayout.Y_AXIS);
        jPanelAddressLeft.setLayout(layoutAddress);

        JPanel jPanelAddressRight = new JPanel();
        layoutAddress = new BoxLayout(jPanelAddressRight, BoxLayout.Y_AXIS);
        jPanelAddressRight.setLayout(layoutAddress);

        jPanelAddressLeft.add(new JLabel("Address Name"));
        jTextFieldAddressName = new JTextField(FIELD_COLUMNS + 5);
        jTextFieldAddressName.setMaximumSize(jTextFieldAddressName.getPreferredSize());
        jPanelAddressLeft.add(jTextFieldAddressName);

        jPanelAddressRight.add(new JLabel("Address Number"));
        jTextFieldAddressNumber = new JTextField(5);
        jTextFieldAddressNumber.setMaximumSize(jTextFieldAddressNumber.getPreferredSize());
        jPanelAddressRight.add(jTextFieldAddressNumber);

        jPanelAddress.add(jPanelAddressLeft);
        jPanelAddress.add(jPanelAddressRight);
        jPanel.add(jPanelAddress);


        jPanel.add(new JLabel("Sold"));

        JPanel jPanelSold = new JPanel();
        BoxLayout layoutSold = new BoxLayout(jPanelSold, BoxLayout.X_AXIS);
        jPanelSold.setLayout(layoutSold);

        jRadioButtonSoldYes = new JRadioButton("Yes");
        jRadioButtonSoldNo = new JRadioButton("No");
        group = new ButtonGroup();
        group.add(jRadioButtonSoldYes);
        group.add(jRadioButtonSoldNo);
        jPanelSold.add(jRadioButtonSoldYes);
        jPanelSold.add(jRadioButtonSoldNo);
        jPanel.add(jPanelSold);

        jPanel.add(new JLabel("Sold date"));
        jTextFieldSoldDate = new JTextField();
        jTextFieldSoldDate = new JTextField(FIELD_COLUMNS);
        jTextFieldSoldDate.setMaximumSize(jTextFieldSoldDate.getPreferredSize());
        jPanel.add(jTextFieldSoldDate);

        JPanel jPanelButtons = new JPanel();
        BoxLayout layoutX3 = new BoxLayout(jPanelButtons, BoxLayout.X_AXIS);
        jPanelButtons.setLayout(layoutX3);

        ImageIcon iconSave = new ImageIcon(getClass().getResource("/Corneliu/images/iconSave.png"));
//        iconSave.setImage(new ImageIcon(getClass().getResource("/chiosa_denis/images/troll.jpg")).getImage());
        jButtonSave = new JButton(iconSave);
        jPanelButtons.add(jButtonSave);

        ImageIcon iconClear = new ImageIcon(getClass().getResource("/Corneliu/images/iconClear.png"));
        jButtonClear = new JButton(iconClear);
        jPanelButtons.add(jButtonClear);

        jPanel.add(jPanelButtons);

        return jPanel;
    }

    private void initWindow() {
        setTitle("Catalog");
        setSize(1000, 400);
        setResizable(false);
        initMenu();
    }

    private void openWindow() {
        Properties properties = new Properties();
        InputStream input= null;

        try {
            File file = new File("config.properties");


            if( !file.exists()){
                setLocationRelativeTo(null);
                return;
            }

                input = new FileInputStream(file);
                properties.load(input);

            if(!properties.containsKey("x")){
                setLocationRelativeTo(null);
                return;
            }
                int x = Integer.parseInt(properties.getProperty("x"));
                int y = Integer.parseInt(properties.getProperty("y"));
                setLocation(x, y);


        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private void closeWindow() {
        Properties properties = new Properties();
        OutputStream output = null;

        try {
            output = new FileOutputStream("config.properties");

            String x = String.valueOf(getLocation().x);
            String y = String.valueOf(getLocation().y);

            properties.setProperty("x", x);
            properties.setProperty("y", y);

            properties.store(output, null);
        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.exit(0);
    }

    private void initMenu() {
        JMenuBar jMenuBar = new JMenuBar();

        JMenu menuFile = new JMenu("File");

        JMenu menuExport = new JMenu("Export");
        menuFile.add(menuExport);

        itemExcelExport = new JMenuItem("Excel");
        itemDatabaseExport = new JMenuItem("Database");

        menuExport.add(itemExcelExport);
        menuExport.add(itemDatabaseExport);

        JMenu menuImport = new JMenu("Import");
        menuFile.add(menuImport);

        itemExcelImport = new JMenuItem("Excel");
        itemDatabaseImport = new JMenuItem("Database");

        menuImport.add(itemExcelImport);
        menuImport.add(itemDatabaseImport);

        menuFile.addSeparator();

        itemExit = new JMenuItem("Exit");
        menuFile.add(itemExit);

        JMenu menuHelp = new JMenu("Help");
        itemAbout = new JMenuItem("About");

        menuHelp.add(itemAbout);

        jMenuBar.add(menuFile);
        jMenuBar.add(menuHelp);

        setJMenuBar(jMenuBar);
    }

    public void initMenuListeners() {
        itemExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        itemAbout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Created by: Cruselnitchi Corneliu");
            }
        });

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                super.windowOpened(e);
                openWindow();
            }

            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                closeWindow();
            }
        });

    }

    public void fillForm(Property property) {
        jTextFieldTypeOfProperty.setText(property.getTypeOfProperty());
        jComboBoxCountRooms.setSelectedIndex(property.getCountRooms());
        jTextFieldPrice.setText(String.valueOf(property.getPrice()));
        jTextFieldAddressName.setText(property.getAdress().getAddressName());
        jTextFieldAddressNumber.setText(String.valueOf(property.getAdress().getAddressNumber()));
        //radiobox
        boolean isSold = property.isSold();
        if (isSold == true) {
            jRadioButtonSoldYes.setSelected(true);
        } else {
            jRadioButtonSoldNo.setSelected(true);
        }

        if(property.getSoldDate() == null){
            jTextFieldSoldDate.setText("");
        } else {
            String zz = String.valueOf(property.getSoldDate().getDayOfMonth());
            String ll = String.valueOf(property.getSoldDate().getMonthValue());
            String aaaa = String.valueOf(property.getSoldDate().getYear());
            String dateFormat = String.format(zz + "." + ll + "." + aaaa);

            jTextFieldSoldDate.setText(dateFormat);
        }
    }
}
