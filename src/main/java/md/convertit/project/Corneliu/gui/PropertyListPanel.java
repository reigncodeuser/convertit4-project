package md.convertit.project.Corneliu.gui;

import md.convertit.project.Corneliu.model.Address;
import md.convertit.project.Corneliu.model.Property;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * Created by Cornel on 25.06.2017.
 */
public class PropertyListPanel extends JPanel {
    private List<Property> propertyList = new ArrayList<>();
    private TableExModel model;
    private JTable jTable;
    private final PropertyCatalog propertyCatalog;

    private String[] columns = {"Type", "Rooms", "Price", "Address name", "Address number", "Sold", "Sold date"};

    public List<Property> getPropertyList() {
        return propertyList;
    }

    public PropertyListPanel(PropertyCatalog propertyCatalog) throws HeadlessException {
        this.propertyCatalog = propertyCatalog;
        initPanel();
        initListeners();
    }

    private void initListeners() {
        jTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int selectedRow = jTable.getSelectedRow();
                    propertyCatalog.fillForm(propertyList.get(selectedRow));

//                    System.out.println(propertyList.get(selectedRow));
                }
            }
        });
    }

    private void initPanel() {
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        model = new TableExModel();

        jTable = new JTable();
        jTable.setModel(model);

        JScrollPane jScrollPane = new JScrollPane(jTable);
        jScrollPane.setHorizontalScrollBar(new JScrollBar(JScrollBar.HORIZONTAL));
        jScrollPane.setVerticalScrollBar(new JScrollBar(JScrollBar.VERTICAL));
        jScrollPane.getVerticalScrollBar().setUnitIncrement(20);

        add(jScrollPane);

        setBackground(Color.WHITE);


    }

    public void saveProperty(Property property) {
        propertyList.add(property);
        model.fireTableDataChanged();
    }

    class TableExModel extends AbstractTableModel {

        public TableExModel() {
            propertyList.add(new Property("Apartament", 3, 4500.25, new Address("Sarmizegetusa", 15), true, LocalDate.of(2015, 06, 15)));
            propertyList.add(new Property("Casa", 5, 8500.99, new Address("Dacia", 26), false, null));
        }

        public List<Property> getPropertyList() {
            return propertyList;
        }

        @Override
        public int getRowCount() {
            return propertyList.size();
        }

        @Override
        public int getColumnCount() {
            return 7;
        }

        @Override
        public String getColumnName(int column) {
            return columns[column];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Property property = propertyList.get(rowIndex);
            switch (columnIndex) {
                case 0: return property.getTypeOfProperty();
                case 1: return property.getCountRooms();
                case 2: return property.getPrice();
                case 3: return property.getAdress().getAddressName();
                case 4: return property.getAdress().getAddressNumber();
                case 5: return property.isSold();
                case 6: return property.getSoldDate();

                default: return "";
            }
        }
    }
}
