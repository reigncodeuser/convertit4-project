package md.convertit.project.Corneliu.services;

import md.convertit.project.Corneliu.model.Address;
import md.convertit.project.Corneliu.model.Property;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Repemol on 07.07.2017.
 */
public class ExcelService {
    List<Property> list = new ArrayList<>();

    public void export(List<Property> list, String fileName) {
        int rowNumber = 0;

        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        Row row = sheet.createRow(rowNumber);
        Cell cell;

        String[] columns = {"Type", "Rooms", "Price", "Address name", "Address number", "Sold", "Sold date"};

        for (int i = 0; i < columns.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(columns[i]);
        }

        for (int i = 0; i < list.size(); i++) {
            //i => index din lista
            Property property = list.get(i);
            row = sheet.createRow(++rowNumber);

            cell = row.createCell(0);
            cell.setCellValue(property.getTypeOfProperty());

            cell = row.createCell(1);
            cell.setCellValue(property.getCountRooms());

            cell = row.createCell(2);
            cell.setCellValue(property.getPrice());

            cell = row.createCell(3);
            cell.setCellValue(property.getAdress().getAddressName());

            cell = row.createCell(4);
            cell.setCellValue(property.getAdress().getAddressNumber());

            cell = row.createCell(5);
            cell.setCellValue(property.isSold());

            cell = row.createCell(6);

            if (property.getSoldDate() == null) {
                cell.setCellValue("");
            } else {
                cell.setCellValue(String.valueOf(property.getSoldDate()));
            }
        }
        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
        sheet.autoSizeColumn(3);
        sheet.autoSizeColumn(4);
        sheet.autoSizeColumn(5);
        sheet.autoSizeColumn(6);

        File file = new File(fileName);
        FileOutputStream outputStream = null;

        try {
            outputStream = new FileOutputStream(file);
            workbook.write(outputStream);
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Am scris in fisier: " + file.getAbsolutePath());
        }

//        System.out.println(list);
//        System.out.println(file);
    }

    public void importFromExcel(String fileName) {
        Property property;
        String type = null;
        int rooms = 0;
        double price = 0;
        String addressName = null;
        int addressNumber = 0;
        boolean sold = false;
        String soldDateString = null;
        LocalDate soldDate = null;

        File file = new File(fileName);
        try {
            FileInputStream fis = new FileInputStream(file);

            Workbook workbook = new HSSFWorkbook(fis);

            Sheet sheet = workbook.getSheetAt(0);

            int countRow = sheet.getLastRowNum();

            for (int i = 1; i <= countRow; i++) {
                Row row = sheet.getRow(i);
                int countCell = row.getLastCellNum();
                for (int j = 0; j < countCell; j++) {
                    Cell cell = row.getCell(j);

                    if (j == 0) {
                        type = cell.getStringCellValue();
                    } else if (j == 1) {
                        rooms = (int) cell.getNumericCellValue();
                    } else if (j == 2) {
                        price = cell.getNumericCellValue();
                    } else if (j == 3) {
                        addressName = cell.getStringCellValue();
                    } else if (j == 4) {
                        addressNumber = (int) cell.getNumericCellValue();
                    } else if (j == 5) {
                        sold = cell.getBooleanCellValue();
                    } else if (j == 6) {
                        soldDateString = cell.getStringCellValue();

                        if (sold != false) {
                            String[] date = soldDateString.split("-");

                            int day = Integer.parseInt(date[2]);
                            int month = Integer.parseInt(date[1]);
                            int year = Integer.parseInt(date[0]);

                            soldDate = LocalDate.of(year, month, day);
                        } else {
                            soldDate = null;
                        }
                    }
                }
                property = new Property(type, rooms, price, new Address(addressName, addressNumber), sold, soldDate);
                list.add(property);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Property> getPropertyList() {
        return list;
    }
}
