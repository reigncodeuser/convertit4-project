package md.convertit.project.Corneliu.model;

import java.time.LocalDate;

/**
 * Created by Repemol on 23.06.2017.
 */
public class Property {
    private Long id;

    private String typeOfProperty;
    private int countRooms;
    private double price;
    private Address adress;
    private boolean sold;
    private LocalDate soldDate;

    public Property() {
    }

    public Property(String typeProperty, int countRooms, double price, Address adress, boolean sold, LocalDate soldDate) {
        this.typeOfProperty = typeProperty;
        this.countRooms = countRooms;
        this.price = price;
        this.adress = adress;
        this.sold = sold;
        this.soldDate = soldDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeOfProperty() {
        return typeOfProperty;
    }

    public void setTypeOfProperty(String typeOfProperty) {
        this.typeOfProperty = typeOfProperty;
    }

    public int getCountRooms() {
        return countRooms;
    }

    public void setCountRooms(int countRooms) {
        this.countRooms = countRooms;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Address getAdress() {
        return adress;
    }

    public void setAdress(Address adress) {
        this.adress = adress;
    }

    public boolean isSold() {
        return sold;
    }

    public void setSold(boolean sold) {
        this.sold = sold;
    }

    public LocalDate getSoldDate() {
        return soldDate;
    }

    public void setSoldDate(LocalDate soldDate) {
        this.soldDate = soldDate;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Property{");
        sb.append("typeOfProperty='").append(typeOfProperty).append('\'');
        sb.append(", countRooms=").append(countRooms);
        sb.append(", price=").append(price);
        sb.append(", adress=").append(adress);
        sb.append(", sold=").append(sold);
        sb.append(", soldDate=").append(soldDate);
        sb.append('}');
        return sb.toString();
    }
}
