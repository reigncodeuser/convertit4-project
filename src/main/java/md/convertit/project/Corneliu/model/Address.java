package md.convertit.project.Corneliu.model;

/**
 * Created by Repemol on 23.06.2017.
 */
public class Address {
    private String addressName;
    private int addressNumber;

    public Address() {
    }

    public Address(String addressName, int addressNumber) {
        this.addressName = addressName;
        this.addressNumber = addressNumber;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public int getAddressNumber() {
        return addressNumber;
    }

    public void setAddressNumber(int addressNumber) {
        this.addressNumber = addressNumber;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Address{");
        sb.append("addressName='").append(addressName).append('\'');
        sb.append(", addressNumber='").append(addressNumber).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
