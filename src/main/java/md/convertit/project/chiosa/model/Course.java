package md.convertit.project.chiosa.model;

/**
 * Created by Utilizator on 23.06.2017.
 */
public class Course {
    private Long id;
    private String name;
    private double price;
    private boolean practiceActive;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isPracticeActive() {
        return practiceActive;
    }

    public void setPracticeActive(boolean practiceActive) {
        this.practiceActive = practiceActive;
    }
}
