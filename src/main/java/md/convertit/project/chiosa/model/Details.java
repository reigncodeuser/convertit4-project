package md.convertit.project.chiosa.model;

import java.time.LocalDate;

/**
 * Created by Utilizator on 23.06.2017.
 */
public class Details {
    private String details;
    private LocalDate updatedOn;

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public LocalDate getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(LocalDate updatedOn) {
        this.updatedOn = updatedOn;
    }
}
