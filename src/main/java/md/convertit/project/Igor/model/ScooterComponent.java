package md.convertit.project.Igor.model;

/**
 * Created by Admin on 28.06.2017.
 */
public class ScooterComponent {

    private String name;
    private Double price;
    private Integer done;

    public ScooterComponent() {
    }

   public ScooterComponent(String name, Double price, Integer done) {
       this.name = name;
       this.price = price;
       this.done = done;
   }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getDone() {
        return done;
    }

    public void setDone(Integer done) {
        this.done = done;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
