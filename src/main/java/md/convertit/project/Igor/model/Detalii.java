package md.convertit.project.Igor.model;

/**
 * Created by user on 23.06.2017.
 */
public class Detalii {

    private String modelRoti;

    private String modelMotor;

    private String modelVolan;

    public Detalii() {

    }

    public Detalii(String modelRoti, String modelMotor, String modelVolan) {
        this.modelRoti = modelRoti;
        this.modelMotor = modelMotor;
        this.modelVolan = modelVolan;
    }

    public String getModelRoti() {
        return modelRoti;
    }

    public void setModelRoti(String modelRoti) {
        this.modelRoti = modelRoti;
    }

    public String getModelMotor() {
        return modelMotor;
    }

    public void setModelMotor(String modelMotor) {
        this.modelMotor = modelMotor;
    }

    public String getModelVolan() {
        return modelVolan;
    }

    public void setModelVolan(String modelVolan) {
        this.modelVolan = modelVolan;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Detalii{");
        sb.append("modelRoti='").append(modelRoti).append('\'');
        sb.append(", modelMotor='").append(modelMotor).append('\'');
        sb.append(", modelVolan='").append(modelVolan).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
