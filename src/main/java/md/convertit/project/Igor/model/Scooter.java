package md.convertit.project.Igor.model;

import java.time.LocalDate;

/**
 * Created by user on 23.06.2017.
 */
public class Scooter {

    private Long id;

    private String model;

    private LocalDate startOrderDate;

    private LocalDate finishOrderDate;

    private Double price;

    private Boolean licenseOn;

    private Detalii detalii;


    public Scooter() {
    }

    public Scooter(String model, LocalDate startOrderDate, Double price, Boolean licenseOn, Detalii detalii) {
        this.model = model;
        this.startOrderDate = startOrderDate;
        this.price = price;
        this.licenseOn = licenseOn;
        this.detalii = detalii;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public LocalDate getStartOrderDate() {
        return startOrderDate;
    }

    public void setStartOrderDate(LocalDate startOrderDate) {
        this.startOrderDate = startOrderDate;
    }

    public LocalDate getFinishOrderDate() {
        return finishOrderDate;
    }

    public void setFinishOrderDate(LocalDate finishOrderDate) {
        this.finishOrderDate = finishOrderDate;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getLicenseOn() {
        return licenseOn;
    }

    public void setLicenseOn(Boolean licenseOn) {
        this.licenseOn = licenseOn;
    }

    public Detalii getDetalii() {
        return detalii;
    }

    public void setDetalii(Detalii detalii) {
        this.detalii = detalii;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Scooter{");
        sb.append("model='").append(model).append('\'');
        sb.append(", startOrderDate=").append(startOrderDate);
        sb.append(", finishOrderDate=").append(finishOrderDate);
        sb.append(", price=").append(price);
        sb.append(", licenseOn=").append(licenseOn);
        sb.append(", detalii=").append(detalii);
        sb.append('}');
        return sb.toString();
    }
}
