package md.convertit.project.Igor.gui;

import md.convertit.project.Igor.model.Scooter;
import md.convertit.project.Igor.gui.model.ScooterTableModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * Created by user on 26.06.2017.
 */
public class MainFrameRightPanel extends JPanel {


    private JTable jTable;

    private JScrollPane jScrollPane;

    private ScooterTableModel myTableModel;

    private final MainFrameScooter mainFrameScooter;


    public void saveScooter(Scooter scooter) {

        myTableModel.getScooterList().add(scooter);
        myTableModel.fireTableDataChanged();
    }


    public MainFrameRightPanel(MainFrameScooter mainFrameScooter) {
        this.mainFrameScooter = mainFrameScooter;

        initRightPanel();

        initRightPanelListeners();
    }

    private void initRightPanelListeners() {
        jTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int selectedRow = jTable.getSelectedRow();

                    mainFrameScooter.populateLeftPanel(myTableModel.getScooterList().get(selectedRow));
                }
            }
        });
    }


    private void initRightPanel() {
        BoxLayout layout = new BoxLayout(this, BoxLayout.X_AXIS);
        setLayout(layout);

        myTableModel = new ScooterTableModel();
        jTable = new JTable(myTableModel);
        jTable.getTableHeader().setToolTipText("THIS IS SPARTA!!!");
        jScrollPane = new JScrollPane(jTable);
        jScrollPane.setViewportView(jTable);
        jScrollPane.getViewport().setBackground(Color.GRAY);
        // jScrollPane.setSize(1000, 170);
        //  jTable.setPreferredScrollableViewportSize(new Dimension(950, 100));
        add(jScrollPane);

    }
}
