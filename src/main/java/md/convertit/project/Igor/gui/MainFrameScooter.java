package md.convertit.project.Igor.gui;


import md.convertit.project.Igor.model.Scooter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.Properties;

/**
 * Created by user on 26.06.2017.
 */
public class MainFrameScooter extends JFrame {

    private JSplitPane jSplitPane;

    private MainFrameLeftPanel leftPanel;
    private MainFrameRightPanel rightPanel;

    private JMenuBar jMenuBar;
    private JMenu jMenuFile;
    private JMenu jMenuHelp;
    private JMenuItem jMenuItemExitFile;
    private JMenuItem itemAbout;


    public MainFrameScooter() throws HeadlessException {

        initWindow();

        initSplitPain();

        initMainPanel();

        initJBarMenu();

        initMenuListeners();

        initWindowListeners();

    }

    public void saveScooter(Scooter scooter) {
        rightPanel.saveScooter(scooter);
    }

    public void populateLeftPanel(Scooter scooter) {
        leftPanel.fillForm(scooter);
    }


    private void initJBarMenu() {

        jMenuBar = new JMenuBar();

        jMenuFile = new JMenu("File");
        jMenuFile.setMnemonic(KeyEvent.VK_F);

        JMenu menuExport = new JMenu("Export");
        JMenuItem itemExcel = new JMenuItem("Excel");
        JMenuItem itemDatabase = new JMenuItem("Database");

        jMenuHelp = new JMenu("Help");
        jMenuHelp.setMnemonic(KeyEvent.VK_H);
        itemAbout = new JMenuItem("About");


        jMenuItemExitFile = new JMenuItem("Exit");
        jMenuItemExitFile.setMnemonic((KeyEvent.VK_E));
        jMenuItemExitFile.setToolTipText("Exit application");


        jMenuFile.add(menuExport);
        menuExport.setToolTipText("???????");
        menuExport.add(itemExcel);
        menuExport.add(itemDatabase);
        jMenuFile.add(jMenuItemExitFile);

        jMenuHelp.add(itemAbout);

        jMenuBar.add(jMenuFile);
        jMenuBar.add(jMenuHelp);
        setJMenuBar(jMenuBar);


    }

    private void initMainPanel() {

        getContentPane().add(jSplitPane);//adaug insasi pe suprafata fereastrei

    }

    private void initSplitPain() {

        jSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);

        leftPanel = new MainFrameLeftPanel(this);//(this) - transmitem acest obiect ca referinta in leftPanel(in situatia cu saveScooter)
        jSplitPane.setLeftComponent(leftPanel);
        rightPanel = new MainFrameRightPanel(this);//(this) - transmitem acest obiect ca referinta in rightPanel(in situatia cu inversarea)
        jSplitPane.setRightComponent(rightPanel);

        jSplitPane.setDividerLocation(200);

        jSplitPane.setEnabled(false);
    }

    private void initWindow() {

        setTitle("Scooter builder");
        setSize(980, 450);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    private void initMenuListeners() {
        jMenuItemExitFile.addActionListener(e -> System.exit(0));

        itemAbout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,
                        "Scooter builder 2017.07.04\nCreated by: Igor Cioroi\nAll rights reserved");
            }
        });
    }

    private void initWindowListeners() {

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                super.windowOpened(e);
                openWindow();
            }

            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                closeWindow();
            }
        });
    }

    private void openWindow() {
        Properties properties = new Properties();
        InputStream input = null;

        try {
            File file = new File("config.properties");


            if (!file.exists()) {
                setLocationRelativeTo(null);
                return;
            }

            input = new FileInputStream(file);
            properties.load(input);

            if (!properties.containsKey("x")) {
                setLocationRelativeTo(null);
                return;
            }
            int x = Integer.parseInt(properties.getProperty("x"));
            int y = Integer.parseInt(properties.getProperty("y"));
            setLocation(x, y);


        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void closeWindow() {

        Properties properties = new Properties();
        OutputStream output = null;

        try {
            output = new FileOutputStream("config.properties");

            String x = String.valueOf(getLocation().x);
            String y = String.valueOf(getLocation().y);

            properties.setProperty("x", x);
            properties.setProperty("y", y);

            properties.store(output, null);
        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.exit(0);
    }
}
