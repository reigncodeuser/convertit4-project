package md.convertit.project.Igor.gui.model;

import md.convertit.project.Igor.model.Scooter;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 30.06.2017.
 */
public class ScooterTableModel extends AbstractTableModel {

    List<Scooter> scooterList;


    public ScooterTableModel() {

        initData();
    }

    public List<Scooter> getScooterList() {
        return scooterList;
    }

    private void initData() {
        scooterList = new ArrayList<>();

        /*scooterList.add(new Scooter("AMERICAN",
                LocalDate.of(1945, 12, 12), 2000D, true,
                new Detalii("German", "Italian", "Englez")));*/
    }

    @Override
    public int getRowCount() {
        return scooterList.size();
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public Object getValueAt(int r, int c) {
        Scooter scooter = this.scooterList.get(r);
        switch (c) {
            case 0:
                return scooter.getModel();
            case 1:
                return scooter.getDetalii().getModelMotor();
            case 2:
                return scooter.getDetalii().getModelVolan();
            case 3:
                return scooter.getDetalii().getModelRoti();
            case 4:
                return scooter.getLicenseOn();
            case 5:
                return String.format("%.2f", scooter.getPrice());
            case 6:
                return scooter.getStartOrderDate();
            case 7:
                return scooter.getFinishOrderDate();
            default:
                return "";
        }
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Model";
            case 1:
                return "Motor";
            case 2:
                return "Volan";
            case 3:
                return "Wheel";
            case 4:
                return "LicenseOn";
            case 5:
                return "Price";
            case 6:
                return "OrderDate";
            case 7:
                return "Will be done";
        }
        return "";
    }
}
