package md.convertit.project.Igor.gui;

import md.convertit.project.Igor.model.Detalii;
import md.convertit.project.Igor.model.Scooter;
import md.convertit.project.Igor.model.ScooterComponent;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.LocalDate;

/**
 * Created by user on 26.06.2017.
 */
public class MainFrameLeftPanel extends JPanel {
    private final int FIELD_COLUMNS = 10;

    private final ScooterComponent[] TYPE = {
            new ScooterComponent("Vespa", 199.99D, 10),
            new ScooterComponent("Lambretta", 333.99D, 12),
            new ScooterComponent("Harley-Davidson", 696D, 15),
            new ScooterComponent("Scooter", 2000D, 20)};

    private final ScooterComponent[] DETAILS = {
            new ScooterComponent("Vauxhall", 57.85D, 5),
            new ScooterComponent("Wilson", 99.99D, 7),
            new ScooterComponent("Scooter", 999D, 10)};

    private JComboBox jComboBoxModel;
    private JComboBox jComboBoxModelRoti;
    private JComboBox jComboBoxModelVolan;
    private JComboBox jComboBoxModelMotor;

    private JTextField jTextFieldOrderDate;
    private JTextField jTextFieldFinishDate;
    private JTextField jTextFieldPrice;

    private JCheckBox jCheckBoxLicense;

    private JButton jButtonSave;
    private JButton jButtonClear;

    private final MainFrameScooter mainFrameScooter;


    public MainFrameLeftPanel(MainFrameScooter mainFrameScooter) {

        this.mainFrameScooter = mainFrameScooter;

        initComponents();

        initListeners();

    }


    private void initComponents() {

        BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
        setLayout(layout);

        JLabel jLabel = new JLabel("Model");
        add(jLabel);
        jComboBoxModel = new JComboBox(TYPE);
        jComboBoxModel.setMaximumSize(jComboBoxModel.getPreferredSize());
        add(jComboBoxModel);


        add(new JLabel("Marca motor"));
        jComboBoxModelMotor = new JComboBox(TYPE);
        jComboBoxModelMotor.setMaximumSize(jComboBoxModelMotor.getPreferredSize());
        add(jComboBoxModelMotor);


        add(new JLabel("Marca volan"));
        jComboBoxModelVolan = new JComboBox(DETAILS);
        jComboBoxModelVolan.setPrototypeDisplayValue("Harrley-Daidson");
        jComboBoxModelVolan.setMaximumSize(jComboBoxModelVolan.getPreferredSize());
        add(jComboBoxModelVolan);


        add(new JLabel("Marca roti"));
        jComboBoxModelRoti = new JComboBox(DETAILS);
        jComboBoxModelRoti.setPrototypeDisplayValue("Harley-Davidson");
        jComboBoxModelRoti.setMaximumSize(jComboBoxModelRoti.getPreferredSize());
        add(jComboBoxModelRoti);


        add(new JLabel("Necesita licenta? :"));
        jCheckBoxLicense = new JCheckBox();
        add(jCheckBoxLicense);


        add(new JLabel("Price"));
        jTextFieldPrice = new JTextField(FIELD_COLUMNS);
        jTextFieldPrice.setMaximumSize(jTextFieldPrice.getPreferredSize());
        jTextFieldPrice.setEditable(false);
        add(jTextFieldPrice);


        add(new JLabel("StartOrderDate? :"));
        jTextFieldOrderDate = new JTextField(FIELD_COLUMNS);
        jTextFieldOrderDate.setMaximumSize(jTextFieldOrderDate.getPreferredSize());
        jTextFieldOrderDate.setText("04.07.2017");
        add(jTextFieldOrderDate);


        add(new JLabel("FinishOrderDate!"));
        jTextFieldFinishDate = new JTextField(FIELD_COLUMNS);
        jTextFieldFinishDate.setMaximumSize(jTextFieldFinishDate.getPreferredSize());
        add(jTextFieldFinishDate);


        JPanel jPanelButtons = new JPanel();

        BoxLayout layoutButtons = new BoxLayout(jPanelButtons, BoxLayout.LINE_AXIS);
        jPanelButtons.setLayout(layoutButtons);


        ImageIcon iconSave = new ImageIcon();
        iconSave.setImage(new ImageIcon(getClass().getResource("/Igor/iconSave.png")).getImage());
        jButtonSave = new JButton(iconSave);
        jPanelButtons.add(jButtonSave);

        ImageIcon iconClear = new ImageIcon();
        iconClear.setImage(new ImageIcon(getClass().getResource("/Igor/iconClear.png")).getImage());
        jButtonClear = new JButton(iconClear);
        jPanelButtons.add(jButtonClear);

        add(jPanelButtons);
    }

    private void initListeners() {

        jButtonSave.addActionListener(e -> {

            boolean isFormValid = validateForm();

            if (isFormValid) {
                saveScooter();
            }
        });
        jButtonClear.addActionListener(e -> clearForm());

    }

    private void clearForm() {

        jComboBoxModel.setSelectedIndex(0);
        jComboBoxModelVolan.setSelectedIndex(0);
        jComboBoxModelRoti.setSelectedIndex(0);
        jComboBoxModelMotor.setSelectedIndex(0);

        jTextFieldOrderDate.setText("");
        jTextFieldFinishDate.setText("");
        jCheckBoxLicense.setSelected(false);
        jTextFieldPrice.setText("0.0");
    }

    private void saveScooter() {

        Double totalPrice = 0.0D;

        Integer totalTime = 0;

        Scooter scooter = new Scooter();

        ScooterComponent model = (ScooterComponent) jComboBoxModel.getSelectedItem();
        scooter.setModel(model.getName());
        totalPrice += model.getPrice();
        totalTime += model.getDone();

        String[] dataAsString = jTextFieldOrderDate.getText().split("\\.");
        LocalDate localDate = LocalDate.of(
                Integer.parseInt(dataAsString[2]),
                Integer.parseInt(dataAsString[1]),
                Integer.parseInt(dataAsString[0]));
        scooter.setStartOrderDate(localDate);


        Detalii detalii = new Detalii();
        ScooterComponent marcaVolan = (ScooterComponent) jComboBoxModelVolan.getSelectedItem();
        detalii.setModelVolan(marcaVolan.getName());
        totalPrice += marcaVolan.getPrice();
        totalTime += marcaVolan.getDone();


        ScooterComponent marcaMotor = (ScooterComponent) jComboBoxModelMotor.getSelectedItem();
        detalii.setModelMotor(marcaMotor.getName());
        totalPrice += marcaMotor.getPrice();
        totalTime += marcaMotor.getDone();

        ScooterComponent marcaRoti = (ScooterComponent) jComboBoxModelRoti.getSelectedItem();
        detalii.setModelRoti(marcaRoti.getName());
        totalPrice += marcaRoti.getPrice();
        totalTime += marcaRoti.getDone();


        Boolean isLicensed = jCheckBoxLicense.isSelected();
        scooter.setLicenseOn(isLicensed);

        if (isLicensed) {
            totalPrice += (totalPrice * 0.2);
        }


        jTextFieldPrice.setText(String.format("%.2f", totalPrice));


        LocalDate finish = localDate.plusDays(totalTime);

        scooter.setFinishOrderDate(finish);

        jTextFieldFinishDate.setText(String.valueOf(finish));

        scooter.setPrice(totalPrice);

        scooter.setDetalii(detalii);

        mainFrameScooter.saveScooter(scooter);

    }


    private boolean validateForm() {
        boolean isValid = true;
        StringBuilder builder = new StringBuilder();
        builder.append("Errors occurred:\n");

        //text fields validate


        String datePattern = "\\d{2}.\\d{2}.\\d{4}";
        String date = jTextFieldOrderDate.getText();
        if (!date.matches(datePattern)) {
            isValid = false;
            String message = "Introduceti data dupa formatul (AAAA.LL.ZZ)";
            builder.append(message + "\n");
        }


        if (!isValid) {
            JOptionPane.showMessageDialog(this, builder.toString(),
                    "Atentie", JOptionPane.ERROR_MESSAGE);
        }

        return isValid;
    }

    public void fillForm(Scooter scooter) {
        setSelectedValue(jComboBoxModel, scooter.getModel());
        setSelectedValue(jComboBoxModelMotor, scooter.getDetalii().getModelMotor());
        setSelectedValue(jComboBoxModelVolan, scooter.getDetalii().getModelVolan());
        setSelectedValue(jComboBoxModelRoti, scooter.getDetalii().getModelRoti());

        jCheckBoxLicense.setSelected(scooter.getLicenseOn());

        jTextFieldPrice.setText(String.format("%.2f", scooter.getPrice()));

        //construim data ca string
        LocalDate localDate = scooter.getStartOrderDate();
        StringBuilder sb = new StringBuilder();
        sb.append(localDate.getDayOfMonth());
        sb.append(".");
        sb.append(localDate.getMonthValue());
        sb.append(".");
        sb.append(localDate.getYear());
        jTextFieldOrderDate.setText(sb.toString());

        LocalDate localDate1 = scooter.getFinishOrderDate();
        StringBuilder sb1 = new StringBuilder();
        sb1.append(localDate1.getDayOfMonth());
        sb1.append(".");
        sb1.append(localDate1.getMonthValue());
        sb1.append(".");
        sb1.append(localDate1.getYear());
        jTextFieldFinishDate.setText(sb1.toString());
    }

    private void setSelectedValue(JComboBox comboBox, String value)
    {
        ScooterComponent item;
        for (int i = 0; i < comboBox.getItemCount(); i++)
        {
            item = (ScooterComponent) comboBox.getItemAt(i);
            if (item.getName().equals(value)){
                comboBox.setSelectedIndex(i);
                break;
            }
        }
    }
}
