package md.convertit.project.dionisie.util;

import md.convertit.project.dionisie.model.BankTranzationDetails;

/**
 * Created by d.comerzan on 7/25/2017.
 */
public class ComisionUtil {



    public static void getComision(BankTranzationDetails bankTranzationDetails){

        double currentAmount = bankTranzationDetails.getAmountTransfer();
        String residentClient = bankTranzationDetails.getResidentClient();
        String residentBeneficiary = bankTranzationDetails.getResidentBeneficiary();
        int comision;
        double comisionForAmount = 0;

        if (currentAmount < 500000 && residentClient.equalsIgnoreCase("Rezident") && residentBeneficiary.equalsIgnoreCase("Rezident")){
            comision = 1;
            comisionForAmount = currentAmount * comision / 100;
        }
        bankTranzationDetails.setComisionForAmount(comisionForAmount);
        //return comisionForAmount;
    }

    public static void getAmountWithComision(BankTranzationDetails bankTranzationDetails) {
        getComision(bankTranzationDetails);

        //double currentComision = bankTranzationDetails.getComisionForAmount()//getComision(bankTranzationDetails);
        double currentAmount = bankTranzationDetails.getAmountTransfer();
        double amountWithComision = bankTranzationDetails.getComisionForAmount() + currentAmount;
        bankTranzationDetails.setAmountWithComision(amountWithComision);

        //return amountWithComision;

    }



}
