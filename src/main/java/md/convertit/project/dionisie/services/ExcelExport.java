package md.convertit.project.dionisie.services;

import md.convertit.project.dionisie.model.BankTranzationDetails;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by d.comerzan on 7/26/2017.
 */
public class ExcelExport {
    public void export(List<BankTranzationDetails> list, String fileName) {
        int rowNumber = 0;

        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        Row row = sheet.createRow(rowNumber);
        Cell cell;

        String[] columns = {"Data", "Nume client", "Prenume client", "Cont client", "Nume beneficiar", "Prenume beneficiar", "Cont beneficiar", "Suma transferată", "Comision", "Suma extrasă"};

        for (int i = 0; i < columns.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(columns[i]);
        }

        for (int i = 0; i < list.size(); i++) {

            BankTranzationDetails bankTranzationDetails = list.get(i);
            row = sheet.createRow(++rowNumber);

            cell = row.createCell(0);
            cell.setCellValue(String.valueOf(bankTranzationDetails.getDayOfTranzaction()));

            cell = row.createCell(1);
            cell.setCellValue(bankTranzationDetails.getLastNameClient());

            cell = row.createCell(2);
            cell.setCellValue(bankTranzationDetails.getFirstNameClient());

            cell = row.createCell(3);
            cell.setCellValue(bankTranzationDetails.getNumberCountClient());

            cell = row.createCell(4);
            cell.setCellValue(bankTranzationDetails.getLastNameBeneficiary());

            cell = row.createCell(5);
            cell.setCellValue(bankTranzationDetails.getFirstNameBeneficiary());

            cell = row.createCell(6);
            cell.setCellValue(bankTranzationDetails.getNumberCountBeneficiary());

            cell = row.createCell(7);
            cell.setCellValue(bankTranzationDetails.getAmountTransfer());

            cell = row.createCell(8);
            cell.setCellValue(bankTranzationDetails.getComisionForAmount());

            cell = row.createCell(9);
            cell.setCellValue(bankTranzationDetails.getAmountWithComision());
        }

        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
        sheet.autoSizeColumn(3);
        sheet.autoSizeColumn(4);
        sheet.autoSizeColumn(5);
        sheet.autoSizeColumn(6);
        sheet.autoSizeColumn(7);
        sheet.autoSizeColumn(8);
        sheet.autoSizeColumn(9);

        File file = new File(fileName);
        FileOutputStream outputStream = null;

        try {
            outputStream = new FileOutputStream(file);
            workbook.write(outputStream);
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("S-a inscris in fisier: " + file.getAbsolutePath());
        }


    }
}
