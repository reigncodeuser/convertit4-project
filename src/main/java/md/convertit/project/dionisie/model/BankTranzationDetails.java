package md.convertit.project.dionisie.model;

import java.time.LocalDate;


/**
 * Created by Dionisie Comerzan on 23.06.2017.
 */
public class BankTranzationDetails {
    private Long id;
    private String lastNameClient;
    private String firstNameClient;
    private int numberCountClient;
    private double amountTransfer;
    private String lastNameBeneficiary;
    private String firstNameBeneficiary;
    private int numberCountBeneficiary;
    private Boolean confirmation;
    private LocalDate dayOfTranzaction;
       //private BankComision bankComision;
    private String residentClient;
    private String residentBeneficiary;

    private double comisionForAmount;
    private double amountWithComision;

    public BankTranzationDetails() {
    }

    /*public BankTranzationDetails(String lastNameClient, String firstNameClient, int numberCountClient, double amountTransfer, String lastNameBeneficiary, String firstNameBeneficiary, int numberCountBeneficiary) {
        this.lastNameClient = lastNameClient;
        this.firstNameClient = firstNameClient;
        this.numberCountClient = numberCountClient;
        this.amountTransfer = amountTransfer;
        this.lastNameBeneficiary = lastNameBeneficiary;
        this.firstNameBeneficiary = firstNameBeneficiary;
        this.numberCountBeneficiary = numberCountBeneficiary;
    }*/

    public double getComisionForAmount() {
        return comisionForAmount;
    }

    public void setComisionForAmount(double comisionForAmount) {
        this.comisionForAmount = comisionForAmount;
    }

    public double getAmountWithComision() {
        return amountWithComision;
    }

    public void setAmountWithComision(double amountWithComision) {
        this.amountWithComision = amountWithComision;
    }



    public Boolean getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(Boolean confirmation) {
        this.confirmation = confirmation;
    }

    public int getNumberCountClient() {
        return numberCountClient;
    }

    public void setNumberCountClient(int numberCountClient) {
        this.numberCountClient = numberCountClient;
    }

    public int getNumberCountBeneficiary() {
        return numberCountBeneficiary;
    }

    public void setNumberCountBeneficiary(int numberCountBeneficiary) {
        this.numberCountBeneficiary = numberCountBeneficiary;
    }

    public double getAmountTransfer() {
        return amountTransfer;
    }

    public void setAmountTransfer(double amountTransfer) {
        this.amountTransfer = amountTransfer;
    }

    public String getLastNameClient() {
        return lastNameClient;
    }

    public void setLastNameClient(String lastNameClient) {
        this.lastNameClient = lastNameClient;
    }

    public String getFirstNameClient() {
        return firstNameClient;
    }

    public void setFirstNameClient(String firstNameClient) {
        this.firstNameClient = firstNameClient;
    }

    public String getLastNameBeneficiary() {
        return lastNameBeneficiary;
    }

    public void setLastNameBeneficiary(String lastNameBeneficiary) {
        this.lastNameBeneficiary = lastNameBeneficiary;
    }

    public String getFirstNameBeneficiary() {
        return firstNameBeneficiary;
    }

    public void setFirstNameBeneficiary(String firstNameBeneficiary) {
        this.firstNameBeneficiary = firstNameBeneficiary;
    }

    public LocalDate getDayOfTranzaction() {
        return dayOfTranzaction;
    }

    public void setDayOfTranzaction(LocalDate dayOfTranzaction) {
        this.dayOfTranzaction = dayOfTranzaction;
    }

    /*public BankComision getBankComision() {
        return bankComision;
    }

    public void setBankComision(BankComision bankComision) {
        this.bankComision = bankComision;
    }*/

    public String getResidentClient() {
        return residentClient;
    }

    public void setResidentClient(String residentClient) {
        this.residentClient = residentClient;
    }

    public String getResidentBeneficiary() {
        return residentBeneficiary;
    }

    public void setResidentBeneficiary(String residentBeneficiary) {
        this.residentBeneficiary = residentBeneficiary;
    }

    @Override
    public String toString() {
        return "BankTranzationDetails{" +
                "lastNameClient='" + lastNameClient + '\'' +
                ", firstNameClient='" + firstNameClient + '\'' +
                ", numberCountClient=" + numberCountClient +
                ", amountTransfer=" + amountTransfer +
                ", lastNameBeneficiary='" + lastNameBeneficiary + '\'' +
                ", firstNameBeneficiary='" + firstNameBeneficiary + '\'' +
                ", numberCountBeneficiary=" + numberCountBeneficiary +
                ", confirmation=" + confirmation +
                ", dayOfTranzaction=" + dayOfTranzaction +
                '}';
    }
}



