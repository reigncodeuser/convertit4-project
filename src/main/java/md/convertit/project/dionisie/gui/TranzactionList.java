package md.convertit.project.dionisie.gui;

import md.convertit.project.dionisie.model.BankTranzationDetails;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dionisie Comerzan on 30.06.2017.
 */
public class TranzactionList extends AbstractTableModel {
    List<BankTranzationDetails> historyList;

    public TranzactionList() {

        intiHistoryList();

    }

    public void addToHistory(BankTranzationDetails details) {
        historyList.add(details);
        fireTableDataChanged();
    }

    public List<BankTranzationDetails> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List<BankTranzationDetails> historyList) {
        this.historyList = historyList;
    }

    private void intiHistoryList() {

        historyList = new ArrayList<>();
        //historyList.add(new BankTranzationDetails
               // ("Andrei",  2000, 2000.0, "Colea", 2000));
    }

    @Override
    public int getRowCount() {
        return historyList.size();
    }

    @Override
    public int getColumnCount() {
        return 10;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        BankTranzationDetails bankTranzationDetails = this.historyList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return bankTranzationDetails.getDayOfTranzaction();
            case 1:
                return bankTranzationDetails.getLastNameClient();
            case 2:
                return bankTranzationDetails.getFirstNameClient();
            case 3:
                return bankTranzationDetails.getNumberCountClient();
            case 4:
                return bankTranzationDetails.getLastNameBeneficiary();
            case 5:
                return bankTranzationDetails.getFirstNameBeneficiary();
            case 6:
                return bankTranzationDetails.getNumberCountBeneficiary();
            case 7:
                return bankTranzationDetails.getAmountTransfer();
            case 8:
                return bankTranzationDetails.getComisionForAmount();
            case 9:
                return bankTranzationDetails.getAmountWithComision();

            default:
                return "";
        }
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Data";
            case 1:
                return "Nume client";
            case 2:
                return "Prenume client";
            case 3:
                return "Cont client";
            case 4:
                return "Nume beneficiar";
            case 5:
                return "Prenume beneficiar";
            case 6:
                return "Cont beneficiar";
            case 7:
                return "Suma transfer";
            case 8:
                return "Comision";
            case 9:
                return "Suma extrasa";
        }
        return "";
    }
}
