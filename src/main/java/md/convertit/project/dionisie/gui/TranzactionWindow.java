package md.convertit.project.dionisie.gui;

import md.convertit.project.dionisie.model.BankTranzationDetails;
import md.convertit.project.dionisie.util.ComisionUtil;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.Arrays;

/**
 * Created by Dionisie Comerzan on 27.06.2017.
 */
public class TranzactionWindow extends JFrame{
    private final int LENGHT_FIELD = 30;
    private JMenu jMenuFile;
    private JMenu jMenuExport;
    private JMenu jMenuImport;
    private JMenu jMenuHelp;
    private JMenuBar jMenuBar;
    private JMenuItem jMenuItemExcelImport;
    private JMenuItem jMenuItemExcelExport;
    private JMenuItem jMenuItemDatabaseImport;
    private JMenuItem jMenuItemDatabaseExport;
    private JMenuItem jMenuItemAbout;
    private JMenuItem jMenuItemExit;
    private JPanel leftPanel;
    private JPanel rightPanelTop;
    private JPanel rightPanelBottom;
    private JSplitPane splitPane;
    private JSplitPane splitRightPanel;
    private JLabel labelLastNameClient;
    private JLabel labelFirstNameClient;
    private JLabel labelLastNameBeneficiary;
    private JLabel labelFirstNameBeneficiary;
    private JLabel labelAmound;
    private JLabel labelResidentsClient;
    private JLabel labelResidentsBeneficiary;
    private JLabel labelCountClient;
    private JLabel labelCountBenefiaciary;
    private JTextField textFieldLastNameClient;
    private JTextField textFieldFirstNameClient;
    private JTextField textFieldLastNameBeneficiary;
    private JTextField textFieldFirstNameBeneficiary;
    private JTextField textFieldAmound;
    private JTextField textFieldCountClient;
    private JTextField textFieldCountBeneficiary;
    private JTextArea textAreaMessages;
    private JButton buttonSend;
    private JButton buttonCancel;
    private JComboBox comboBoxResidentsClient;
    private JComboBox comboBoxResidentsBeneficiary;
    private JRadioButton radioButtonConfirmDate;
    private JTable jTableHistory;
    private JScrollPane scrollPaneBottom;
    private String [] typeClient;
    private String [] typeBeneficiary;
    private TranzactionList tranzactionList;
    private StringBuilder stringBuilderMessages;
    private ComisionUtil comisionUtil;
    private final String ONLY_STRING = "^[^\\d]+$";
    private final String ONLY_INT =  "^[\\d\\s]+$";

    public TranzactionWindow()throws HeadlessException {
        initWindow();
        initMeniu();
        initLeftPanel();
        initRightPanel();
        intiSplitPanel();
        initListiner();
    }

    //adaugam meniu
    private void initMeniu() {
        // adaugam bara
        jMenuBar = new JMenuBar();

        // adaugam meniu file si punem pe bara
        jMenuFile = new JMenu("File");
        jMenuBar.add(jMenuFile);

        // adaugam meniu import si punem in interiorul meniu file
        jMenuImport = new JMenu("Import");
        jMenuFile.add(jMenuImport);

        // adaugam meniu export si punem in interiorul meniu file
        jMenuExport = new JMenu("Export");
        jMenuFile.add(jMenuExport);

        // adaugam meniu help si punem pe bara
        jMenuHelp = new JMenu("Ajutor");
        jMenuBar.add(jMenuHelp);

        // adaugam submeniul Import Excell
        jMenuItemExcelImport = new JMenuItem("Excel");
        jMenuImport.add(jMenuItemExcelImport);

        // adaugam submeniul Import Database
        jMenuItemDatabaseImport = new JMenuItem("Baza de date");
        jMenuImport.add(jMenuItemDatabaseImport);

        // adugam submeniul Export excel
        jMenuItemExcelExport = new JMenuItem("Excel");
        jMenuExport.add(jMenuItemExcelExport);

        // adugam submeniul Export database
        jMenuItemDatabaseExport = new JMenuItem("Baza de date");
        jMenuExport.add(jMenuItemDatabaseExport);

        // adaugam submeniul Despre in Help
        jMenuItemAbout = new JMenuItem("Despre");
        jMenuHelp.add(jMenuItemAbout);

        // adaugam separator in meniul file
        jMenuFile.addSeparator();

        // adaugam submeniul exit in file
        jMenuItemExit = new JMenuItem("Ieșire");
        jMenuFile.add(jMenuItemExit);

        // setam bara meniu pe fereastra
        setJMenuBar(jMenuBar);
    }

    //initierea panelului drept
    private void initRightPanel() {
        //cream Panelul Top
        rightPanelTop = new JPanel();
        BoxLayout boxLayout1 =new BoxLayout(rightPanelTop, BoxLayout.Y_AXIS);
        rightPanelTop.setLayout(boxLayout1);

        //cream Panelul Bottom
        rightPanelBottom = new JPanel();
        BoxLayout boxLayout2 =new BoxLayout(rightPanelBottom, BoxLayout.Y_AXIS);
        rightPanelBottom.setLayout(boxLayout2);

        // adaugam Panelul Top si Bottom cu SplitPane
        splitRightPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        splitRightPanel.setTopComponent(rightPanelTop);
        splitRightPanel.setBottomComponent(rightPanelBottom);
        splitRightPanel.setDividerLocation(150);

        //spatiul pentru mesaj in panelul Top
        textAreaMessages = new JTextArea();
        rightPanelTop.add(textAreaMessages);

        //initiem Tabelul
        jTableHistory = new JTable();
        tranzactionList = new TranzactionList();
        jTableHistory.setModel(tranzactionList);

        //initiem scrollPane pentru panel Bottom
        scrollPaneBottom = new JScrollPane(jTableHistory);
        scrollPaneBottom.setViewportView(jTableHistory);
        scrollPaneBottom.getViewport().setBackground(Color.GRAY);

        //adaugam scrollPane
        rightPanelBottom.add(scrollPaneBottom);
    }

    //initierea panelului stang
    private void initLeftPanel() {
        //Panelul stang
        leftPanel = new JPanel();
        BoxLayout boxLayout2 = new BoxLayout(leftPanel, BoxLayout.Y_AXIS);
        leftPanel.setLayout(boxLayout2);

        //Adaugm Label Nume client
        labelLastNameClient = new JLabel("Nume client");
        leftPanel.add(labelLastNameClient);

        //Adaugam TextField Nume client
        textFieldLastNameClient = new JTextField(LENGHT_FIELD);
        textFieldLastNameClient.setMaximumSize(textFieldLastNameClient.getPreferredSize());
        leftPanel.add(textFieldLastNameClient);

        //Adaugam Label Prenumele clientului
        labelFirstNameClient = new JLabel("Prenume client");
        leftPanel.add(labelFirstNameClient);

        //Adaugam TextField Prenume client
        textFieldFirstNameClient = new JTextField(LENGHT_FIELD);
        textFieldFirstNameClient.setMaximumSize(textFieldFirstNameClient.getPreferredSize());
        leftPanel.add(textFieldFirstNameClient);

        //Adaugam label contul clientului
        labelCountClient = new JLabel("Nr. cont client");
        leftPanel.add(labelCountClient);

        //Adaugam textfield contul clientului
        textFieldCountClient = new JTextField(LENGHT_FIELD);
        textFieldCountClient.setMaximumSize(textFieldCountClient.getPreferredSize());
        leftPanel.add(textFieldCountClient);

        //Adaugam label la ComboBox -> residents
        labelResidentsClient = new JLabel("Tip client");
        leftPanel.add(labelResidentsClient);

        //Adaugam ComboBox pentru tipul clientului rezident, nerezident
        typeClient = new String[]{"Rezident", "Nerezident"};
        comboBoxResidentsClient = new JComboBox(typeClient);
        comboBoxResidentsClient.setMaximumSize(textFieldLastNameClient.getPreferredSize());
        leftPanel.add(comboBoxResidentsClient);

        //Adaugam Label Nume beneficiar
        labelLastNameBeneficiary = new JLabel("Nume beneficiar");
        leftPanel.add(labelLastNameBeneficiary);

        //Adaugam TextField beneficiar
        textFieldLastNameBeneficiary = new JTextField(LENGHT_FIELD);
        textFieldLastNameBeneficiary.setMaximumSize(textFieldLastNameBeneficiary.getPreferredSize());
        leftPanel.add(textFieldLastNameBeneficiary);

        //Adaugam Label Prenume beneficiar
        labelFirstNameBeneficiary = new JLabel("Prenume beneficiar");
        leftPanel.add(labelFirstNameBeneficiary);

        //Adaugam TextField prenume beneficiar
        textFieldFirstNameBeneficiary = new JTextField(LENGHT_FIELD);
        textFieldFirstNameBeneficiary.setMaximumSize(textFieldFirstNameBeneficiary.getPreferredSize());
        leftPanel.add(textFieldFirstNameBeneficiary);

        //Adaugam label contul clientului
        labelCountBenefiaciary = new JLabel("Nr. cont beneficiar");
        leftPanel.add(labelCountBenefiaciary);

        //Adaugam textfield contul beneficiar
        textFieldCountBeneficiary = new JTextField(LENGHT_FIELD);
        textFieldCountBeneficiary.setMaximumSize(textFieldCountClient.getPreferredSize());
        leftPanel.add(textFieldCountBeneficiary);

        //Adaugam label la combobox tipul beneficiarului
        labelResidentsBeneficiary = new JLabel("Tip beneficiar");
        leftPanel.add(labelResidentsBeneficiary);

        //Adaugam Combobox tipul beneficiarului
        typeBeneficiary = new String[] {"Rezident", "Nerezident"};
        comboBoxResidentsBeneficiary = new JComboBox(typeBeneficiary);
        comboBoxResidentsBeneficiary.setMaximumSize(textFieldLastNameClient.getPreferredSize());
        leftPanel.add(comboBoxResidentsBeneficiary);//Adaugam label contul clientului

        //Adaugam Label nr.2 -> Suma de transferat
        labelAmound = new JLabel("Suma de transferat");
        leftPanel.add(labelAmound);

        //Adaugam TextField nr.2 -> Suma de transferat
        textFieldAmound = new JTextField(LENGHT_FIELD);
        textFieldAmound.setMaximumSize(textFieldAmound.getPreferredSize());
        leftPanel.add(textFieldAmound);

        //Adaugam RadioButton confirmarea datelor
        radioButtonConfirmDate = new JRadioButton("Confirm datele");
        leftPanel.add(radioButtonConfirmDate);

        //Adaugam butonul send
        buttonSend = new JButton("Transfera");
        leftPanel.add(buttonSend);

        //Adaugam butonul cancel
        buttonCancel = new JButton("Anuleaza ");
        leftPanel.add(buttonCancel);
    }

    private void initListiner() {
        buttonSend.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Save");
                boolean isFormValid = validateForm();

                if (isFormValid){
                    saveTranzaction();
                }
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Cancel");
                clearForm();
            }
        });

    }

    private void saveTranzaction() {
        BankTranzationDetails bankTranzationDetails = new BankTranzationDetails();

        String lastNameClient = textFieldLastNameClient.getText();
        bankTranzationDetails.setLastNameClient(lastNameClient);

        String firstNameCLient = textFieldFirstNameClient.getText();
        bankTranzationDetails.setFirstNameClient(firstNameCLient);

        String lastNameBeneficiary = textFieldLastNameBeneficiary.getText();
        bankTranzationDetails.setLastNameBeneficiary(lastNameBeneficiary);

        String firstNameBeneficiary = textFieldFirstNameBeneficiary.getText();
        bankTranzationDetails.setFirstNameBeneficiary(firstNameBeneficiary);

        Double amount = Double.parseDouble(textFieldAmound.getText());
        bankTranzationDetails.setAmountTransfer(amount);

        int numberCountClient = Integer.parseInt(textFieldCountClient.getText());
        bankTranzationDetails.setNumberCountClient(numberCountClient);

        int numberCountBeneficiary = Integer.parseInt(textFieldCountBeneficiary.getText());
        bankTranzationDetails.setNumberCountBeneficiary(numberCountBeneficiary);

        radioButtonConfirmDate.isSelected();
        bankTranzationDetails.setConfirmation(true);

        String residentConfirmationClient = String.valueOf(comboBoxResidentsClient.getSelectedItem());
        bankTranzationDetails.setResidentClient(residentConfirmationClient);

        String residentConfirmationBeneficiary = String.valueOf(comboBoxResidentsBeneficiary.getSelectedItem());
        bankTranzationDetails.setResidentBeneficiary(residentConfirmationBeneficiary);

        bankTranzationDetails.setDayOfTranzaction(LocalDate.now());

        ComisionUtil.getComision(bankTranzationDetails);
        ComisionUtil.getAmountWithComision(bankTranzationDetails);

        displayInvoice(bankTranzationDetails);
        tranzactionList.addToHistory(bankTranzationDetails);

        clearForm();
    }

    private void displayInvoice(BankTranzationDetails bankTranzationDetails) {
        stringBuilderMessages = new StringBuilder();
        stringBuilderMessages.append("Buna ziua, ");
        stringBuilderMessages.append(bankTranzationDetails.getLastNameClient());
        stringBuilderMessages.append(" ");
        stringBuilderMessages.append(bankTranzationDetails.getFirstNameClient());
        stringBuilderMessages.append("\n");
        stringBuilderMessages.append("Ati transeferat de pe contul nr. ");
        stringBuilderMessages.append(bankTranzationDetails.getNumberCountClient());
        stringBuilderMessages.append("\n");
        stringBuilderMessages.append("La data de ");
        //stringBuilderMessages.append(bankTranzationDetails.getDayOfTranzaction());
        stringBuilderMessages.append(bankTranzationDetails.getDayOfTranzaction().getDayOfMonth());
        stringBuilderMessages.append(".");
        stringBuilderMessages.append(bankTranzationDetails.getDayOfTranzaction().getMonthValue());
        stringBuilderMessages.append(".");
        stringBuilderMessages.append(bankTranzationDetails.getDayOfTranzaction().getYear());
        stringBuilderMessages.append("\n");
        stringBuilderMessages.append("Catre ");
        stringBuilderMessages.append(bankTranzationDetails.getLastNameBeneficiary());
        stringBuilderMessages.append(" ");
        stringBuilderMessages.append(bankTranzationDetails.getFirstNameBeneficiary());
        stringBuilderMessages.append("\n");
        stringBuilderMessages.append("La contul nr. ");
        stringBuilderMessages.append(bankTranzationDetails.getNumberCountBeneficiary());
        stringBuilderMessages.append("\n");
        stringBuilderMessages.append("Suma de ");
        stringBuilderMessages.append(bankTranzationDetails.getAmountTransfer());
        stringBuilderMessages.append(" lei");
        stringBuilderMessages.append("\n");
        stringBuilderMessages.append("S-a aplicat un comision de ");
        stringBuilderMessages.append(bankTranzationDetails.getComisionForAmount());
        stringBuilderMessages.append(" lei");
        stringBuilderMessages.append("\n");
        stringBuilderMessages.append("De pe cont s-a extras suma totală de ");
        stringBuilderMessages.append(bankTranzationDetails.getAmountWithComision());
        stringBuilderMessages.append(" lei");
        stringBuilderMessages.append("\n");
        stringBuilderMessages.append("Vă mulțumim pentru tranzacție!");



        textAreaMessages.setText(stringBuilderMessages.toString());
    }

    private boolean validateForm() {
        boolean isValid = true;
        StringBuilder avertisment = new StringBuilder();
        avertisment.append("Atentie!:\n");

        //validerea numelui clientului
        String lastNameClient = textFieldLastNameClient.getText();
        if (lastNameClient.length() == 0 || !lastNameClient.matches(ONLY_STRING)) {
            isValid = false;
            String mesage = "Verifica numele personal";
            avertisment.append(mesage + "\n");
        }

        //validarea prenumemelui clientului
        String firstNameClient = textFieldFirstNameClient.getText();
        if (firstNameClient.length() == 0 || !firstNameClient.matches(ONLY_STRING)) {
            isValid = false;
            String mesage = "Verifica prenumele personal";
            avertisment.append(mesage + "\n");
        }

        //validarea numelui beneficiarului
        String lastNameBeneficiary = textFieldLastNameBeneficiary.getText();
        if (lastNameBeneficiary.length() == 0 || !lastNameBeneficiary.matches(ONLY_STRING)) {
            isValid = false;
            String mesage = "Verifica numele beneficiarului";
            avertisment.append(mesage + "\n");
        }

        //validarea prenumelui beneficiarului
        String firstNameBeneficiary = textFieldFirstNameClient.getText();
        if (firstNameBeneficiary.length() == 0 || !firstNameBeneficiary.matches(ONLY_STRING)) {
            isValid = false;
            String mesage = "Verifica prenumele beneficiarului";
            avertisment.append(mesage + "\n");
        }

        //validarea sumei
        String amound = textFieldAmound.getText();
        if (amound.length() == 0 || !amound.matches(ONLY_INT)) {
            isValid = false;
            String mesage = "Verifica suma introdusa";
            avertisment.append(mesage + "\n");
        }

        String countClient = textFieldCountClient.getText();
        if (countClient.length() == 0 || !countClient.matches(ONLY_INT)) {
            isValid = false;
            String mesage = "Verifica contul personal";
            avertisment.append(mesage + "\n");
        }

        String countBeneficiary = textFieldCountBeneficiary.getText();
        if (countBeneficiary.length() == 0 || !countBeneficiary.matches(ONLY_INT)) {
            isValid = false;
            String mesage = "Verifica contul beneficiarului";
            avertisment.append(mesage + "\n");
        }

        if (!radioButtonConfirmDate.isSelected()) {
            isValid = false;
            String mesage = "Confirma datele introduse";
            avertisment.append(mesage + "\n");
        }

        if (!isValid) {
            JOptionPane.showMessageDialog(this, avertisment.toString(),
                    "Introduce corect datele", JOptionPane.ERROR_MESSAGE);
        }

        return isValid;

    }

    private void clearForm() {
        textFieldAmound.setText("");
        textFieldCountClient.setText("");
        textFieldLastNameBeneficiary.setText("");
        textFieldFirstNameBeneficiary.setText("");
        textFieldCountBeneficiary.setText("");
        textFieldLastNameClient.setText("");
        textFieldFirstNameClient.setText("");
        comboBoxResidentsClient.setSelectedIndex(0);
        comboBoxResidentsBeneficiary.setSelectedIndex(0);
        radioButtonConfirmDate.setSelected(false);
    }

    private void initWindow() {
        setTitle("Online Bank Tranzaction");
        setSize(1000,1000);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    private void intiSplitPanel() {
        //cream SplitPane
        splitPane = new JSplitPane();
        splitPane.setDividerLocation(200);
        splitPane.setLeftComponent(leftPanel);
        splitPane.setRightComponent(splitRightPanel);
        getContentPane().add(splitPane);










    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TranzactionWindow{");
        sb.append("LENGHT_FIELD=").append(LENGHT_FIELD);
        sb.append(", leftPanel=").append(leftPanel);
        sb.append(", splitPane=").append(splitPane);
        sb.append(", labelLastNameClient=").append(labelLastNameClient);
        sb.append(", labelFirstNameClient=").append(labelFirstNameClient);
        sb.append(", labelLastNameBeneficiary=").append(labelLastNameBeneficiary);
        sb.append(", labelFirstNameBeneficiary=").append(labelFirstNameBeneficiary);
        sb.append(", labelAmound=").append(labelAmound);
        sb.append(", labelResidentsClient=").append(labelResidentsClient);
        sb.append(", labelResidentsBeneficiary=").append(labelResidentsBeneficiary);
        sb.append(", labelCountClient=").append(labelCountClient);
        sb.append(", labelCountBenefiaciary=").append(labelCountBenefiaciary);
        sb.append(", textFieldLastNameClient=").append(textFieldLastNameClient);
        sb.append(", textFieldLastNameBeneficiary=").append(textFieldLastNameBeneficiary);
        sb.append(", textFieldAmound=").append(textFieldAmound);
        sb.append(", textFieldCountClient=").append(textFieldCountClient);
        sb.append(", textFieldCountBeneficiary=").append(textFieldCountBeneficiary);
        sb.append(", buttonSend=").append(buttonSend);
        sb.append(", buttonCancel=").append(buttonCancel);
        sb.append(", comboBoxResidentsClient=").append(comboBoxResidentsClient);
        sb.append(", comboBoxResidentsBeneficiary=").append(comboBoxResidentsBeneficiary);
        sb.append(", radioButtonConfirmDate=").append(radioButtonConfirmDate);
        sb.append(", typeClient=").append(typeClient == null ? "null" : Arrays.asList(typeClient).toString());
        sb.append(", typeBeneficiary=").append(typeBeneficiary == null ? "null" : Arrays.asList(typeBeneficiary).toString());
        sb.append('}');
        return sb.toString();
    }
}

