package md.convertit.project.Dima.Model;

/**
 * Created by altavista on 6/23/2017.
 */
public class Details {

    private int ram;
    private double procesor;

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public double getProcesor() {
        return procesor;
    }

    public void setProcesor(double procesor) {
        this.procesor = procesor;
    }
}
