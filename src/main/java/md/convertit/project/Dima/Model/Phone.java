package md.convertit.project.Dima.Model;

import java.time.LocalDate;

/**
 * Created by altavista on 6/23/2017.
 */
public class Phone {

    private Long id;
    private int producere;
    private boolean operator;
    private int pret;
    private LocalDate SoldDate;
    private Details details;

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public int getPret() {
        return pret;
    }

    public void setPret(int pret) {
        this.pret = pret;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getProducere() {
        return producere;
    }

    public void setProducere(int producere) {
        this.producere = producere;
    }

    public boolean isOperator() {
        return operator;
    }

    public void setOperator(boolean operator) {
        this.operator = operator;
    }

    public LocalDate getSoldDate() {
        return SoldDate;
    }

    public void setSoldDate(LocalDate soldDate) {
        SoldDate = soldDate;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Phone{");
        sb.append("id=").append(id);
        sb.append(", producere=").append(producere);
        sb.append(", operator=").append(operator);
        sb.append(", pret=").append(pret);
        sb.append(", SoldDate=").append(SoldDate);
        sb.append(", details=").append(details);
        sb.append('}');
        return sb.toString();
    }
}
