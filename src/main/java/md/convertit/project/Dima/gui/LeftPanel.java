package md.convertit.project.Dima.gui;

import javafx.scene.layout.Pane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.Date;

/**
 * Created by altavista on 6/26/2017.
 */
public class LeftPanel extends JPanel {
    private JButton jButtonSave;
    private JButton jButtonClear;
    private JTextField jTextFieldAnProducere;

    public LeftPanel() {
        setBackground(Color.WHITE);
        initComponents();
        initListener();


    }

    private void initComponents() {
        BoxLayout boxLayout = new BoxLayout(this, BoxLayout.PAGE_AXIS);
        setLayout(boxLayout);
        add(new JLabel("An Producere"));
        jTextFieldAnProducere = new JTextField(15);
        jTextFieldAnProducere.setMaximumSize(new Dimension(100, 30));
        add(jTextFieldAnProducere);
        add(new JLabel("CPU(Ghz)"));
        JTextField jTextFieldprocesor = new JTextField(7);
        jTextFieldprocesor.setMaximumSize(new Dimension(200, 30));
        add(jTextFieldprocesor);
        add(new JLabel("Ram"));
        JTextField jTextFieldRam = new JTextField(7);
        jTextFieldRam.setMaximumSize(new Dimension(200, 30));
        add(jTextFieldRam);


        JPanel jPanelNetwork = new JPanel();
        BoxLayout boxLayoutNetWork = new BoxLayout(jPanelNetwork, BoxLayout.LINE_AXIS);
        jPanelNetwork.setLayout(boxLayoutNetWork);
        jPanelNetwork.add(new JLabel("4G"));
        JCheckBox jCheckBox = new JCheckBox();
        jPanelNetwork.add(jCheckBox);
        jPanelNetwork.add(new JLabel("3G"));
        JCheckBox jCheckBox1 = new JCheckBox();
        jPanelNetwork.add(jCheckBox1);
        add(jPanelNetwork);

        add(new JLabel("Pret (Lei)"));
        JTextField jTextFieldPret = new JTextField(7);
        jTextFieldPret.setMaximumSize(new Dimension(200, 30));
        add(jTextFieldPret);

        add(new JLabel("Sold Date"));
        JTextField jTextSoldDate = new JTextField(7);
        jTextSoldDate.setMaximumSize(new Dimension(200, 30));
        add(jTextSoldDate);
        JPanel jPanel = new JPanel();
        BoxLayout boxLayout1 = new BoxLayout(jPanel, BoxLayout.X_AXIS);
        // todo ImageIcon imageClear = new ImageI
        jButtonClear = new JButton("Clear");
        jPanel.add(jButtonClear);

        jButtonSave = new JButton("Save");
        jPanel.add(jButtonSave);
        add(jPanel);

    }

    private boolean validateForm() {
        boolean isValid = true;
        String anProducere = jTextFieldAnProducere.getText();
        StringBuilder sb = new StringBuilder();
        if (anProducere.length() <4 ) {
            isValid = false;
            sb.append("Verificati An Producere\n");
            //to do validare la textfielduri ramase
        }


        if (!isValid) {
            JOptionPane.showMessageDialog(null,sb.toString());
        }
        return (isValid);

    }

        private void  initListener () {
        jButtonSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Am apasat Save");
                validateForm();


            }
        });

            jButtonClear.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    System.out.println("Am apasat Clear");


                }
            });

    }


    }
