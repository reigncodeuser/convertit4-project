package md.convertit.project.Dima.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by altavista on 6/26/2017.
 */
public class Catalog extends JFrame {
    JSplitPane jSplitPane;
    LeftPanel leftPanel;
    RightPanel rightPanel;




    public Catalog() throws HeadlessException {

      initWindow();
      initPanels();
      initListener();

    }

    private void initListener() {

    }

    private void initPanels() {
        //todo cream JSPLItpane
        jSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        //todo cream left panel
        leftPanel = new LeftPanel();

        //il adaugam pe splitPane la stinha
        jSplitPane.setLeftComponent(leftPanel);

        //cream right panel
        rightPanel = new RightPanel();
        //il adaugam pe JSPlitpane la dreapta
        jSplitPane.setRightComponent(rightPanel);
        //adaugam split pane pe fereastra
        jSplitPane.setDividerLocation(200);
        jSplitPane.setEnabled(false);
        getContentPane().add(jSplitPane);



    }

    private void initWindow() {
        //setam marimi, exit on close
        setTitle("Catalog");
        setSize(400,500);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);





    }
}
