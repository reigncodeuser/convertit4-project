package md.convertit.project.Corneliu.services;

import junit.framework.TestCase;
import md.convertit.project.Corneliu.model.Address;
import md.convertit.project.Corneliu.model.Property;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Repemol on 07.07.2017.
 */
public class ExcelServiceTest extends TestCase {
    public void testExport() throws Exception {
        List<Property> list = new ArrayList<>();
        list.add(new Property("Casa", 5, 4500.25, new Address("Mihai Eminescu", 5), false, null));
        list.add(new Property("Apartament", 3, 4500.25, new Address("Sarmizegetusa", 15), true, LocalDate.of(2015, 06, 15)));

        ExcelService excelService = new ExcelService();
        excelService.export(list, "Property.xls");
    }

}